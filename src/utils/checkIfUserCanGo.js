export let whoWontGoAndWhy = []
export let badPlacesToGo = [];

let arraysHaveCommonElems = (arr1, arr2) => { // check if the arrays have common elements
  return (
    arr1.some(r => arr2.includes(r)) // by checking wether or not at least one item in the first array is included in the second array
  )
}

let arraysAreIdentical = (arr1, arr2) => { // check if the arrays are identical
  return (
    arr1.every(e => arr2.includes(e)) // by checking wether or not every item in the first array is included in the second array
  )
}

let userCanEatSomething = (wont_eat, food) => { // based on the list of food that a user cannot eat and a list of food that a place is serving
  return ((wont_eat.length >= food.length && wont_eat.includes(food)) || arraysAreIdentical(wont_eat, food)); // check if 1. the list of wont eat food is larger than the list of food in the resaturant. 2. if the wont eat list includes all elements of the food list and 3. if the two arrays are identical
};

function checkIfUserCanGo(users, venues) { // check if a user can go to a venue
  users.forEach(user => { // for each user,
    venues.forEach(venue => { // and for each venue
      if (!userCanEatSomething(user.wont_eat, venue.food)) { // check if the user can eat something based on his list of wont eat and the food list of the place
        whoWontGoAndWhy.push(user.label + ' cannot go to ' + venue.name + ' because he has nothing to eat') // if the user wont be able to eat anything, push this sentence into the whoWontGoAndWhy array
        badPlacesToGo.push(venue.name); // and also populate the badPlacesToGo array with the venue name, because the group wont be able to go there
        return;
      } 
      if (!arraysHaveCommonElems(user.drinks, venue.drinks)) { // check if the user can drink something based on the list of every user drinks and the drinks list of the place
        whoWontGoAndWhy.push(user.label + ' cannot go to ' + venue.name + ' because has has nothing to drink') // if the user cannot drink anything, then push this sentence into the array because the group wont go to that place
        badPlacesToGo.push(venue.name); // and also populate the badPlacesToGo array with the venue
        return;
      }
    })
  })
}

export default checkIfUserCanGo;
