import Enzyme, { shallow } from 'enzyme';
import React from 'react';
import Result from '../components/Result/index';
import Adapter from 'enzyme-adapter-react-16';
import expect from 'expect';


Enzyme.configure({ adapter: new Adapter() })

const users = [
  {
    "name": "John Davis",
    "wont_eat": ["Fish"],
    "drinks": ["Cider", "Rum", "Soft drinks"]
  },
  {
    "name": "Gary Jones",
    "wont_eat": ["Eggs", "Pasta"],
    "drinks": ["Tequila", "Soft drinks", "beer", "Coffee"]
  }
]

const venues = [
  {
    "name": "El Cantina",
    "food": ["Mexican"],
    "drinks": ["Soft drinks", "Tequila", "Beer"]
  },
  {
    "name": "Twin Dynasty",
    "food": ["Chinese"],
    "drinks": ["Soft Drinks", "Rum", "Beer", "Whisky", "Cider"]
  }
]

describe('<Result />', () => {
  it('renders three <NameList /> components', () => {
    const wrapper = shallow(<Result users={users} venues={venues} />);
    expect(wrapper.props().children.length).toBe(3);
  });
});