import React from "react";
import ReactDOM from "react-dom";
import SelectMembers from "./components/SelectMembers/index";

const Index = () => {
  return (
    <SelectMembers />
  );
};

ReactDOM.render(<Index />, document.getElementById("index"));