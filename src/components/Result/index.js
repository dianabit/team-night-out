import React from 'react';
import checkIfUserCanGo, { whoWontGoAndWhy, badPlacesToGo } from '../../utils/checkIfUserCanGo';
import NameList from '../NameList';
import List from '../List';

function Result(props) {
  const { venues, users } = props;
  
  checkIfUserCanGo(users, venues);
  const uniqueBadPlacesToGo = [...new Set(badPlacesToGo)] // make a copy of the array that removed duplicates
  const goodPlacesToGo = venues.filter(venue => !uniqueBadPlacesToGo.includes(venue.name)); // get the good places to go by removing the badPlacesToGo array from the original array

  return (
    <React.Fragment>
      <NameList
        text="You can go to the following venues:"
        itemList={goodPlacesToGo}
        decisitionText="Sorry, guys. No places for you to go to."
      />
      <List
        text="You should avoid the following venues:"
        itemList={uniqueBadPlacesToGo}
        decisitionText="Yay! You have many options to choose from."
      />
      <List
        text="The reasons you should avoid the above venues:"
        itemList={whoWontGoAndWhy}
        decisitionText="No reason for you to avoid any venue."
      />
    </React.Fragment>
  )
}

export default Result;