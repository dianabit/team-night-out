import React from 'react';
import styled from 'styled-components';
import Select from 'react-select';
import makeAnimated from 'react-select/lib/animated';
import { users } from '../../data/users';
import { venues } from '../../data/venues';
import ResultList from '../Result/index';

const StyledSelect = styled(Select)`
  width: 50%;
`

class SelectMembers extends React.Component {
constructor(props) {
  super(props);

  this.state = {
    selectedUsers: [],
    multiValue: [],
    readyToRenderList: false,
  }

  this.handleMultiChange = this.handleMultiChange.bind(this);
  this.handleOnButtonClick = this.handleOnButtonClick.bind(this);
}


shouldComponentUpdate(nextProps, nextState) {
  if (this.state.multiValue.length !== nextState.multiValue.length) {
    return true;
  } return false;
}

handleMultiChange(option) {  
  const userNames = users.map((user, index) => ({
    value: index,
    label: user.name,
    wont_eat: user.wont_eat,
    drinks: user.drinks,
  }));
  this.setState({ multiValue: option, readyToRenderList: true, selectedUsers: userNames})
}

handleOnButtonClick() {
  this.setState({ multiValue: [] })
}

  render() {
    const userNames = users.map((user, index) => ({
      value: index,
      label: user.name,
      wont_eat: user.wont_eat,
      drinks: user.drinks,
    }));

    const { multiValue, readyToRenderList } = this.state;

    return (
      <div>
        <h1>Please select members that will join team night out.</h1>
        <StyledSelect
          closeMenuOnSelect={false}
          components={makeAnimated()}
          value={multiValue}
          onChange={this.handleMultiChange}
          isClearable={false}
          clearableValue={false}
          isMulti
          options={userNames}
        />
        {readyToRenderList && (
          <ResultList
            users={multiValue}
            venues={venues}
          />
        )}
      </div>
    )
  }
}

export default SelectMembers;