import React from 'react';

function NameList(props) {
  const { text, itemList, decisitionText } = props;

  return (
    <React.Fragment>
      {itemList.length ? (
        <div>
          <h3>{text}</h3>
          <ul>
          {itemList.map((item, k) => (
            <li key ={k}>{item.name}</li>
          ))}   
        </ul>
      </div>
      ) : (
        <h5>{decisitionText}</h5>
      )}
      </React.Fragment>
  )
}

export default NameList;