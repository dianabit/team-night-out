Clone the source locally:
$ git clone https://dianabit@bitbucket.org/dianabit/team-night-out.git
$ cd team-night-out

Use your package manager to install npm.
$ sudo apt-get install npm nodejs-legacy

Install project dependencies:
$ npm install

Start the app:
$ npm run start

Test the app:
$ npm run jest

Usage:
1. Select some team members that would go out
2. Three lists will be displayed: places they could go, places they should avoid and the reasons to avoid them.

Working time: aprox. 2 hours

Things I would do to improve the program:

1. For each user and venue, I would remove empty characters and make all strings to lower case in their wont_eat, drinks and food arrays, because right now the function doesn't take that into consideration and doesn't display the proper results.

2. Prevent the array of reasons to get populated for each onBlur event on the Select component (the solution would be to clear whoWontGoAndWhy and badPlacesToGo arrays before adding new elements to them)

3. For all the three lists, I would make removing elements dynamic (now only works for adding members). The solution would be to trigger a change of state also when removing elements in SelectMembers.

4. Write more tests; the next one that I would write would be for the function checkIfUserCanEatSomething(users, venues)with mock up functions in jest. I would test that when passing two arrays of strings as parameters for the logical functions, the functions will filter the proper results and tests will pass.

I think this idea has future potential for a useful application that would allow teams and groups of friends to keep track of the good venues they visited in the past. This way, they would avoid making the same mistakes when choosing a place to go out. The app would require a database behind it, with a few relational data tables and some APIs. I would use Redux and axios to consume them and display the data.